#include <stdio.h>
#include <stdint.h>

#include "mex.h"

/*
 * Compile with:
 * $(CC) gcc -I $(MATLABROOT)/extern/include/ -shared -fpic memlayout.c -o memlayout.mexa64
 */

typedef struct {
	double real, imag;
} Complex;

void mexFunction(int nlhs, mxArray **lhs, int nrhs, const mxArray **rhs) {
	if (nrhs < 1) {
		return;
	}

	if (!mxIsComplex(rhs[0])) {
		return;
	}

	int64_t elems = mxGetNumberOfElements(rhs[0]);
	Complex *data = (Complex*)mxGetData(rhs[0]);

	if (!data) {
		printf("Expected doubles, got something else");
		return;
	}

	for (int i = 0; i < elems; i++) {
		printf("[%4i] = %3.0f+%3.0fi\n", i, data[i].real, data[i].imag);
	}
}

