# Rustmex Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.3] 2022-08-05

### Added
1. `assert` and `error_on` macro, which can be used to assert that some condition is true
   and/or error if some condition is not met. This hides some branches, and should make
   cod which uses this library cleaner.

## [0.3.2] 2022-08-05

### Added
1. Implement `MexMessage` for NDarray's `ShapeError` to make working with NDarray a bit
   more ergonomic.

## [0.3.1] 2022-08-05
### Fixes
1. Dependency on rustmex_entrypoint (was still 0.1)

## [0.3.0] 2022-08-05
Mostly a rework of the error handling, partly to make it more ergonomic, partly to
prevent undue memory leaks.

### Added
1. The `rustmex::Result` type, used with the `Error` object.

### Changed
1. `Error` type, now a wrapper around a `MexMessage` trait object instead of an enum.

## [0.2.0] 2022-08-03

### Added
1. Conversions from `mxArray` to (C)String
2. `Function<_>` smart pointer, for wrapping `mxArrays` which hold `function_handle`s
3. The `Missing` trait, for ergonomically handling missing input variables.
4. Library prelude with important functionality.
5. Functionality to call named Matlab functions (without a function handle).

### Fixed
1. Do not allocate space on `mxArray` initialisation (15a1d91)
2. Check sparsity before converting (15a1d91)

## [0.1.2] 2022-06-28
### Added
1. Owned versions of `mxArray` (`MxArray`), returned when by functions which create
   `mxArray`s. When these are dropped, `DestroyArray` is called, ensuring they're freed
   correctly
2. Conversion from `Vec`-ish types to `mxArrays` (15a1d91)
