#![doc = include_str!("../README.md")]

#[cfg(not(any(feature = "matlab_interleaved", feature = "matlab_separated", feature = "octave")))]
compile_error!("Pick one of the 'matlab_interleaved', 'matlab_separated', or 'octave' features");

#[cfg(
	all(
		any(
			all(feature = "matlab_interleaved", feature = "matlab_separated"),
			all(feature = "matlab_interleaved", feature = "octave"),
			all(feature = "octave", feature = "matlab_separated")
		),
		not(feature="doc")
	)
)]
compile_error!("Can't compile Rustmex for multiple API's at the same time. Pick one of 'matlab_interleaved', 'matlab_separated', and 'octave'");

pub mod mex;
pub mod convert;
pub mod message;
pub mod function;
mod mappable;

// as to not have to dig it out of mex or num_complex every time
pub use mex::{raw::mxArray, FromMatlabError, ToMatlabError};
pub use ::num_complex::Complex;
pub use mex::pointers::MxArray;
pub use message::{MexMessage, Error, Missing};

/// The "left hand side" of a call to a mex function. In this slice, the return values
/// should be placed.
///
/// The 'matlab lifetime means that the thing comes from matlab. The 'mex lifetime means
/// that it lives for as long as the 'mex call lasts.
pub type Lhs<'mex> = &'mex mut [Option<MxArray>];

/// The "Right hand side" of a call to a mex function. These are the arguments provided to
/// it in matlab.
///
/// the mxArray here lives for as long as "matlab" "does", since returning it to matlab
/// ensures that it lives for as long as needed --- matlab takes responsibility for
/// managing its memory.
pub type Rhs<'mex, 'matlab> = &'mex [&'matlab mxArray];

/// Re-export the macro to annotate the entry point.
#[cfg(feature = "entrypoint")]
pub use rustmex_entrypoint::entrypoint;

/**
 * Convenience type for returning a Error containing a MexMessage in the error path.
 *
 * Generating a Matlab error diverges; it does not return to Rust code. This prevents
 * destructors from running, and may thus leak memory. Returning from the entrypoint with
 * this type's `Err` variant is therefore preferable.
 */
pub type Result<T> = std::result::Result<T, message::Error>;

/**
 * As is convention in Rust, Rustmex defines a prelude to easily import often used
 * functionality.
 */
pub mod prelude {
	// Since this is a prelude, it only re-exports other things. It should be kept as
	// small as possible. If new items are added, add a short description as to why
	// these items are important enough to be included in the prelude.
	pub use super::{
		// Easy definition of the entrypoint. Do not add message::Error, or
		// crate::Result, since they might conflict with the standard library
		// types.
		Lhs, Rhs,

		// Easy argument parsing
		Missing,

		// These are the basically the core of the api
		mxArray, MxArray,

		// Traits to allow for direct easy conversions into data types, a pretty
		// important part of the library. While, e.g. functions are nice, not
		// everyone wants to use them. Everyone runs into these.
		convert::{FromMatlab, ToMatlab, VecToMatlab, VecType},
	};
}

use std::fmt::Display;

/**
 * Generate an ad-hoc warning from an id and a message
 *
 * See also [message::warning].
 */
pub fn warning<Id, Msg>(id: Id, msg: Msg) -> ()
where
	Id: AsRef<str>,
	Msg: Display
{
	message::warning(&message::AdHoc(id, msg))
}

/**
 * Generate an ad-hoc error from an id and a message.
 *
 * Note that this function diverges, as it returns control to the Matlab prompt. This
 * divergent behaviour prevents destructors from running, and therefore calling this
 * method may leak memory. Consider returning from your entrypoint with a
 * [`rustmex::Result::Err`](crate::Result) instead.
 *
 * See also [message::error].
 */
#[deprecated(since = "0.3.2", note = "Triggering a Matlab error leaks memory since destructors don't run; a convenience function to trigger one is therefore not warranted.")]
pub fn error<Id, Msg>(id: Id, msg: Msg) -> !
where
	Id: AsRef<str>,
	Msg: Display
{
	message::error(&message::AdHoc(id, msg))
}

/**
 * Check some boolean condition; if it is not met, return an Err(Error) with the given id
 * and message. Can be used within functions which return a
 * [`rustmex::Result`](Result).
 */
#[macro_export]
macro_rules! assert {
	($condition:expr, $id:expr, $msg:expr) => {
		::rustmex::error_on!(!$condition, $id, $msg);
	}
}

/**
 * Check some boolean condition, if it is met, return an Err(Error) with the given id and
 * message. Can be used within functions which return a [`rustmex::Result`](Result).
 */
#[macro_export]
macro_rules! error_on {
	($cond:expr, $id:expr, $msg:expr) => {
		if $cond {
			Err(::rustmex::message::AdHoc($id, $msg))?;
		}
	}
}
