/*!
 * Matlab handles errors in a variety of ways, but all of them ultimately boil down to
 * error/warning id plus a message. This module describes that way of error handling in a
 * more Rusty way.
 */

use std::ffi::CString;
use std::ops::Deref;

use std::fmt::{self, Display};

use ndarray::{ShapeError, ErrorKind};

use crate::{
	mex::{
		FromMatlabError,
		ToMatlabError,
	},
	mex::pointers::MxArray,
	mex::raw::mxArray,
};

/**
 * Trait describing a "mex message", i.e. the input to mex's warning and error functions.
 *
 * The methods return &str, because this trait is meant to be used within Rust library
 * code. Having this trait return C string pointers is too much of a burden on the users;
 * conversions to c strings for matlab are done by rustmex
 */
pub trait MexMessage: Display {
	fn id(&self) -> &str;
}

pub struct Error(Box<dyn MexMessage>);

impl Deref for Error {
	type Target = dyn MexMessage;

	fn deref<'a>(&'a self) -> &'a Self::Target {
		&*self.0
	}
}

impl AsRef<dyn MexMessage> for Error {
	fn as_ref<'a>(&'a self) -> &'a (dyn MexMessage + 'static) {
		&*self.0
	}
}

impl<T> From<T> for Error where T: MexMessage + 'static {
	fn from(other: T) -> Self {
		Error(Box::new(other))
	}
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct MissingVariable {
	id: &'static str,
	msg: &'static str,
}

/**
 * One of the first things a MEX file has to do is parse its arguments — the LHS array.
 * Part of that is checking whether it got all the arguments it expected. In MEX files,
 * this is best done via slice's `.get()` method. This method then returns an Option;
 * Some if there is an argument, None if that slot is out of bounds. The value can then
 * be pattern matched out via a `let val = if let Some(_)`, but that requires an `else`
 * arm and is overall just combersome.
 *
 * This trait is the solution for that problem. It elegantly maps an `Option` to a
 * `Result`; an `Ok` if there was some value, an `Err` with a [`MissingVariable`]
 * [`MexMessage`] if not.
 *
 * Note that there is a slight difference in functionality between the two implementers.
 */
pub trait Missing<T> {
	fn error_if_missing(self, id: &'static str , msg: &'static str)
		-> Result<T, MissingVariable> where Self: Sized;
}

/**
 * See [`Missing`] for the main explanation. This implementation also dereferences the
 * indirection of the slice, yielding a direct reference to the
 * [`mxArray`](crate::mex::raw::mxArray).
 *
 * Best used with `get` on a slice.
 */
impl<'a> Missing<&'a mxArray> for Option<&'_ &'a mxArray> {
	fn error_if_missing(self, id: &'static str, msg: &'static str)
		-> Result<&'a mxArray, MissingVariable>
	{
		match self {
			Some(v) => Ok(*v),
			None => Err(MissingVariable {
				id, msg
			})
		}
	}
}

/**
 * See [`Missing`] for the main explanation. Contrast with the implementation for
 * Option<&&mxArray>. This implementation does not dereference the slice's reference,
 * since [`MxArray`] does not implement copy —­The slice remains the owner of the
 * [`MxArray`].
 *
 * Best used with `get_mut` on a slice, and `replace` on the reference yielded to place
 * the new value.
 */
impl<'s> Missing<&'s mut Option<MxArray>> for Option<&'s mut Option<MxArray>> {
	fn error_if_missing(self, id: &'static str, msg: &'static str)
		-> Result<&'s mut Option<MxArray>, MissingVariable>
	{
		match self {
			Some(v) => Ok(v),
			None => Err(MissingVariable {
				id, msg
			})
		}
	}
}

impl MexMessage for MissingVariable {
	fn id(&self) -> &str {
		self.id
	}
}

impl Display for MissingVariable {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.msg)
	}
}

impl MexMessage for FromMatlabError {
	fn id(&self) -> &str {
		match self {
			Self::BadClass => "rustmex:convert:bad_class",
			Self::BadComplexity => "rustmex:convert:bad_complexity",
			Self::BadSparsity => "rustmex:convert:bad_sparsity",
			Self::Size => "rustmex:convert:size_mismatch"
		}
	}
}

impl Display for FromMatlabError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		// TODO: This should probably a bit more specific, but the enum atm does
		// not store the source mxArray nor the target type, so we can't (yet).
		write!(f, "Error converting mxArray to rust type")
	}
}

impl MexMessage for ToMatlabError {
	fn id(&self) -> &str {
		match self {
			Self::MismatchedSize => "rustmex:to_matlab:mismatched_size",
			Self::ComplexSizeMismatch => "rustmex:to_matlab:complex_size_mismatch",
		}
	}
}

impl Display for ToMatlabError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let msg = match self {
			Self::MismatchedSize => "shape does not agree with data size",
			Self::ComplexSizeMismatch => "real and imaginary parts do not have equal lengths"
		};

		write!(f, "{msg}")
	}
}

pub struct AdHoc<Id, Msg>(pub Id, pub Msg);

/**
 * Convenience implementation for MexMessage for two str's. Sometimes an ad-hoc
 * error/warning is generated, from two strings instead of a specific error type.
 */
impl<Id, Msg> MexMessage for AdHoc<Id, Msg> where Id: AsRef<str>, Msg: Display {
	fn id(&self) -> &str { self.0.as_ref() }
}

impl<Id, Msg> Display for AdHoc<Id, Msg> where Msg: Display {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { self.1.fmt(f) }
}

/**
 * Generate a Matlab warning
 */
pub fn warning(w: &dyn MexMessage) -> () {
	let id = CString::new(w.id()).expect("No inner nul bytes");
	let msg = CString::new(w.to_string()).expect("No inner nul bytes");
	unsafe { super::mex::raw::mexWarnMsgIdAndTxt(id.as_ptr(), msg.as_ptr()); }
}

/**
 * Generate a Matlab error
 *
 * Note that this function diverges, as it returns control to the Matlab prompt. This
 * divergent behaviour prevents destructors from running, and therefore calling this
 * method may leak memory. Consider returning from your entrypoint with a
 * [`rustmex::Result::Err`](crate::Result) instead.
 */
pub fn error(e: &dyn MexMessage) -> ! {
	let id = CString::new(e.id()).expect("No inner nul bytes");
	let msg = CString::new(e.to_string()).expect("No inner nul bytes");

	// In an attempt to minimize memory leakage, copy the id and message (which are
	// persistent allocations) into a nonpersistent cstr.
	let id = crate::mex::alloc::into_nonpersistent_cstr(id);
	let msg = crate::mex::alloc::into_nonpersistent_cstr(msg);

	unsafe { super::mex::raw::mexErrMsgIdAndTxt(id, msg); }
	unreachable!();
}

impl MexMessage for ShapeError {
	fn id(&self) -> &str {
		use ErrorKind::*;
		match self.kind() {
			IncompatibleShape  => "ndarray:incompatible_shape",
			IncompatibleLayout => "ndarray:incompatible_layout",
			RangeLimited	   => "ndarray:range_limited",
			OutOfBounds        => "ndarray:out_of_bounds",
			Unsupported        => "ndarray:unsupported",
			Overflow           => "ndarray:overflow",
			_                  => "rustmex:ndarray:unrecognised_errorkind",
		}
	}
}
