use std::ops::{Deref, DerefMut, Drop};
use std::clone::Clone;
use std::borrow::{ToOwned, Borrow};
use std::convert::AsRef;

use std::ptr::NonNull;

use super::raw::{
	mxDuplicateArray,
	mxDestroyArray,

	mxSetData,
	mxCreateUninitNumericArray,

	mwSize,
	mxClassID,
	mxComplexity,
};

use super::{
	MatlabPrimitive,

	ToMatlabError,

	mxArray,
};

#[allow(unused)]
use super::MatlabComplex;

#[allow(unused)]
use num_complex::Complex;

use cfg_if::cfg_if;

#[cfg(not(feature = "matlab_interleaved"))]
use super::raw::{
	mxSetImagData,
};

#[cfg(not(feature = "matlab_interleaved"))]
use crate::mappable::{Mappable, RefMappable};

/**
 * A Matlab Pointer is a marker trait, used to be generic over pointers which can be used
 * across API boundaries with Matlab. Two of these are already implemented (Matlab
 * returns an opaque pointer to objects, so these can just be used as-is as references),
 * which leaves the third — the owned type — to be implemented. _See_ [MxArray].
 */
pub trait MatlabPtr: Deref<Target = mxArray> {}
impl<'a> MatlabPtr for &'a mut mxArray { }
impl<'a> MatlabPtr for &'a mxArray { }
impl MatlabPtr for MxArray {}

/**
 * The owned variant of an mxArray. Some of Matlab's mex functions specify that the
 * caller is responsible for deallocating the object when it is done with it, unless the
 * object in question is returned to Matlab. This cannot be expressed with just (mutable)
 * references, so this type implements the owned type.
 *
 * It is basically a wrapper around a mutable reference to an mxArray, but it implements
 * the drop trait, so when it is dropped it calls `mxDestroyArray`. It implements
 * [`Deref`] and [`DerefMut`], so it can also be used where-ever a (mutable) reference to
 * an mxArray is expected.
 */
#[repr(transparent)]
pub struct MxArray(NonNull<mxArray>);

impl Drop for MxArray {
	fn drop(&mut self) {
		unsafe { mxDestroyArray(self.0.as_ptr()) };
	}
}

impl Deref for MxArray {
	type Target = mxArray;

	fn deref(&self) -> &Self::Target {
		unsafe { self.0.as_ref() }
	}
}

impl DerefMut for MxArray {
	fn deref_mut(&mut self) -> &mut Self::Target {
		unsafe { self.0.as_mut() }
	}
}

impl mxArray {
	pub fn duplicate(&self) -> MxArray {
		let ptr = unsafe { mxDuplicateArray(self) };
		if ptr.is_null() {
			panic!("OOM");
		}
		unsafe { MxArray::assume_responsibility(&mut *ptr ) }
	}
}

impl Clone for MxArray {
	fn clone(&self) -> Self {
		self.duplicate()
	}
}

impl Borrow<mxArray> for MxArray {
	fn borrow(&self) -> &mxArray {
		self.deref()
	}
}

impl ToOwned for mxArray {
	type Owned = MxArray;

	fn to_owned(&self) -> Self::Owned {
		self.duplicate()
	}
}

impl AsRef<mxArray> for mxArray {
	fn as_ref(&self) -> &mxArray {
		self
	}
}

impl AsRef<mxArray> for MxArray {
	fn as_ref(&self) -> &mxArray {
		self.deref()
	}

}
cfg_if! {
	if #[cfg(feature = "octave")] {
		type ShapePtr = *const mwSize;
	} else {
		type ShapePtr = *mut mwSize;
	}
}

impl MxArray {
	// NOTE: I'm not yet sure whether accepting and returning references from the
	// responsibility handling methods is the best idea. Maybe accepting/returning
	// (NonNull) pointers is better?

	/**
	 * Create an MxArray by assuming responsibility for freeing an mxArray. Unless
	 * you have allocated an mxArray for which you are responsible through calling
	 * any method in the [`mex::raw`](crate::mex::raw) module, you do not need this —
	 * see [`MxArray::new`] or higher level abstractions (such as
	 * [`ToMatlab`](crate::convert::ToMatlab)) instead.
	 */
	pub unsafe fn assume_responsibility(mx: &'static mut mxArray) -> Self {
		Self(NonNull::new_unchecked(mx as *mut mxArray))
	}

	/**
	 * Transfer responsibility for deallocating this mxArray back to Matlab. Unless
	 * you are manually calling functions in [`mex::raw`](crate::mex::raw), you do
	 * not need this.
	 */
	pub unsafe fn transfer_responsibility(o: Self) -> &'static mut mxArray {
		let ptr = o.0.as_ptr();
		std::mem::forget(o);
		&mut *ptr
	}

	/**
	 * Create a new MxArray with some data and a shape.
	 *
	 * If the number of elements of the data does not agree with the product of the
	 * entries in the shape, this function will return an `Err`.
	 *
	 * If the number of elements of the data does not agree with the product of the
	 * entries in the shape, this function will return an `Err`.
	 *
	 * ## Data Length
	 * Most Rust functions return a `usize` for lengths, since a length can never be
	 * negative. This associated function is easiest to use with that use case.
	 *
	 * However, some target API's define their length type to be signed. If
	 * you want to be portable over those differences, use this function with a
	 * `try_from` instead.
	 *
	 * Note that this only matters for very large datasets, 2^63 -1 or greater for 64
	 * bit machines. This function panics if the data's length is larger than that.
	 */
	pub fn new<D, T>(data: D, shape: &[usize]) -> Result<Self, ToMatlabError> where
		D: Into<Box<[T]>>,
		T: MatlabPrimitive
	{
		// NOTE: Octave uses signed integer types for the lengths, while matlab
		// uses unsigned types. In case the smaller type is used, check whether
		// all the usizes fit below the max of the mwSize type. mwSize::MAX can
		// fit in an usize, however, so we can safely upcast there.
		#[cfg(feature = "octave")]
		assert!(shape.iter().all(|&v| v <= mwSize::MAX as usize),
			"Octave uses a signed type for the length of dimensions. One or more values was over this signed type's maximum value."
		);
		Self::new_mwsize(data, unsafe { std::mem::transmute(shape) })
	}

	/**
	 * Create a new MxArray with some data with a given shape.
	 *
	 * See [`MxArray::new`] for the explanation for the difference between it and
	 * this function.
	 *
	 * This function panics if any dimension length in shape is negative (only
	 * applies to target API's where mwSize is signed).
	 */
	pub fn new_mwsize<D, T>(data: D, shape: &[mwSize]) -> Result<Self, ToMatlabError> where
		D: Into<Box<[T]>>,
		T: MatlabPrimitive
	{
		#[cfg(all(not(feature = "doc"), feature = "octave"))]
		assert!(shape.iter().all(|&v| v.is_positive()),
			"Octave uses a signed type for the lengths of dimensions. One or more values in the give shape are negative"
		);

		let data = data.into();
		if shape.iter().product::<mwSize>() != data.len() as mwSize {
			return Err(ToMatlabError::MismatchedSize)
		}


		let mx = unsafe {
			mxCreateUninitNumericArray(
				shape.len() as mwSize,
				shape.as_ptr() as ShapePtr,
				T::CLASS_ID as mxClassID,
				T::COMPLEXITY as mxComplexity)
		};

		// There isn't much for us to do if an allocation request can't be
		// accomodated.
		if mx.is_null() {
			panic!("OOM");
		}

		let ptr = Box::into_raw(data);
		unsafe { mxSetData(mx, ptr as *mut core::ffi::c_void); }

		Ok(unsafe { Self::assume_responsibility(&mut *mx) } )
	}

	/**
	 * Create a new MxArray with a non-interleaved complex dataset and a shape. This
	 * associated function only exists for target API's which do not interleave the
	 * real and imaginary parts of complex numbers. Hence, consider using higher
	 * level abstractions for transforming your data to an MxArray (such as via the
	 * [`crate::convert::ToMatlab`] trait) instead.
	 *
	 * This function panics when any dimension length in shape overflows
	 * [`mwSize::MAX`] for target API's where [`mwSize`] max is not an [`usize`].
	 */
	#[cfg(any(not(feature = "matlab_interleaved"), feature = "doc"))]
	pub fn new_complex<D, T>(data: Complex<D>, shape: &[usize]) -> Result<Self, ToMatlabError> where
		D: Into<Box<[T]>>,
		Complex<T>: MatlabComplex,
		T: MatlabPrimitive
	{
		// NOTE: Octave uses signed integer types for the lengths, while matlab
		// uses unsigned types. In case the smaller type is used, check whether
		// all the usizes fit below the max of the mwSize type. mwSize::MAX can
		// fit in an usize, however, so we can safely upcast there.
		#[cfg(feature = "octave")]
		assert!(shape.iter().all(|&v| v <= mwSize::MAX as usize),
			"Octave uses a signed type for the length of dimensions. One or more values was over this signed type's maximum value."
		);
		Self::new_complex_mwsize(data, unsafe { std::mem::transmute(shape) })
	}


	/**
	 * Non-interleaved analog of [`MxArray::new_mwsize`].
	 *
	 * Panics when any dimension length is negative on target API's where `mwSize` is
	 * signed.
	 */
	#[cfg(any(not(feature = "matlab_interleaved"), feature = "doc"))]
	pub fn new_complex_mwsize<D, T>(data: Complex<D>, shape: &[mwSize]) -> Result<Self, ToMatlabError> where
		D: Into<Box<[T]>>,
		Complex<T>: MatlabComplex,
		T: MatlabPrimitive
	{
		#[cfg(feature = "octave")]
		assert!(shape.iter().all(|&v| v.is_positive()),
			"Octave uses a signed type for the lengths of dimensions. One or more values in the give shape are negative"
		);

		let data = data.map(|x| x.into());
		let lens = data.map_ref(|x| x.len());

		if lens.re != lens.im {
			return Err(ToMatlabError::ComplexSizeMismatch);
		}

		if shape.iter().product::<mwSize>() != lens.re as mwSize {
			return Err(ToMatlabError::MismatchedSize)
		}

		let mx = unsafe {
			mxCreateUninitNumericArray(
				shape.len() as mwSize,
				shape.as_ptr() as ShapePtr,
				T::CLASS_ID as mxClassID,
				true as mxComplexity)
		};

		// There isn't much for us to do if an allocation request can't be
		// accomodated.
		if mx.is_null() {
			panic!("OOM");
		}

		let ptr = data.map(|x| Box::into_raw(x));
		unsafe { mxSetData(mx, ptr.re as *mut core::ffi::c_void); }
		unsafe { mxSetImagData(mx, ptr.im as *mut core::ffi::c_void); }

		Ok( unsafe { Self::assume_responsibility(&mut *mx)})
	}
}
