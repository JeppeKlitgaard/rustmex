import re
import sys

# Small cli program to produce all the lines of symbol renaming for the raw module
#
# It converts all the versioned pub fn's into 'use as' imports, and just imports
# unversioned types and functions unchanged.

pubfn = re.compile(".*pub fn ([a-zA-Z]+)(_800|_730|_700|)\(.*")
pubelse = re.compile("pub type ([a-zA-Z_0-9]+) = ")

for line in sys.stdin:
	if r := pubfn.match(line):
		name, api = r.groups()
		if api:
			print(f"\t{name}{api} as {name},")
		else:
			print(f"\t{name},")
	elif r := pubelse.match(line):
		name,  = r.groups()
		print(f"\t{name},")

