use std::alloc::{GlobalAlloc, Layout};
use std::os::raw::c_void;
use super::raw::{mxMalloc, mxFree, mexMakeMemoryPersistent};

/**
 * Simple allocator which forwards allocation requests the mex allocator.
 *
 * By default, matlab returns a pointer, which memory will be cleared after the mex
 * function returns. This is a problem when some data structure assumes the memory won't
 * be cleared, for example with threads. Since we can rely on Rust's ownership model, it
 * is okay (for the most part) to make every non-null pointer returned persistent.
 *
 * The only exception to this might be mxArray's themselves. These, as of yet, don't have
 * an implementation for Drop. If an mxArray is created, one must manually call
 * [crate::mex::raw::mxDestroyArray] on it. However, generally this shouldn't be a problem, as mxArrays
 * are only created on the API interface and directly returned to Matlab.
 *
 * Note that Rust globals which hold references to the heap will never be deallocated.
 * For instance, Rayon's global thread pool is orchestrated via some global static value.
 * Since the allocations are static, this will live even after the mex function returns.
 * This may or may not be desired behaviour.
 */
// In line with the matlab types (see raw.rs), we make use bad style for the allocator
// struct too.
#[allow(non_camel_case_types)]
pub struct mxAlloc;

#[cfg(any(feature = "alloc", feature = "doc"))]
#[global_allocator]
static ALLOCATOR: mxAlloc = mxAlloc;

unsafe impl GlobalAlloc for mxAlloc {

	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		let ptr = mxMalloc(layout.size());
		// TODO: Put this behind a configuration flag?
		//
		// There might be some implementations which do not want or need this
		// behaviour, and actually do want their globals to be freed. This only
		// works for trivial Drop implementations however; Matlab does not run
		// the destructors for us.
		if !ptr.is_null() {
			mexMakeMemoryPersistent(ptr);
		}
		ptr as *mut u8
	}

	unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
		mxFree(ptr as *mut c_void);
	}
}

/**
 * Some Mex API functions diverge, i.e. they do not return to Rust code. This means that
 * destructors, which we rely on to free memory (since we persist all allocations) do not
 * run.
 *
 * Some of those Mex API functions take a string, such as mexErrMsgIdAndTxt. This
 * function copy's a persistently allocated CString into a nonpersistent buffer, drops
 * the CString (so it's freed before the diverging function is called) and returns a
 * pointer to the nonpersistent buffer.
 */
pub fn into_nonpersistent_cstr(s: std::ffi::CString) -> *mut i8 {
	let v = s.into_bytes_with_nul();
	let l = v.len();

	let ptr = unsafe { mxMalloc(v.len()) as *mut u8 };
	if ptr.is_null() {
		panic!("OOM")
	}

	let sl = unsafe { std::slice::from_raw_parts_mut(ptr, l) };

	sl.copy_from_slice(&v);
	sl.as_mut_ptr() as *mut i8
}
