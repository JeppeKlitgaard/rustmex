/*!
 * This module contains functionality to call back into Matlab. Both calling via a
 * `function_handle` or a named matlab function (such as "sqrt") are supported.
 */
use crate::{
	mex::{
		MatlabClass,
		FromMatlabError,
		ClassID,
		raw::{
			mxArray,
			mexCallMATLAB,
		},
		pointers::{
			MatlabPtr,
			MxArray,
		},
	},
	convert::{
		FromMatlab,
	},
};

use std::{
	ffi::{
		CString,
		CStr,
	}
};

/**
 * Call a named Matlab function, such as `sqrt`, or `fmincon`. If an error occurs,
 * control is returned to the matlab prompt. Note that this might leak memory.
 *
 * Note that this function converts the function name into a CString. If you already have
 * a null-terminated string available, consider using [`call_named_nul`].
 */
pub fn call_named<FnName>(f: FnName, nargout: usize, rhs: &[&mxArray])
	-> Result<Box<[Option<MxArray>]>, FunctionCallError>
where
	FnName: Into<Vec<u8>>,
{
	let f = CString::new(f).map_err(|_| FunctionCallError::BadEncoding)?;
	call_named_nul(&f, nargout, rhs).ok_or(FunctionCallError::CallFailed)
}

/**
 * Call a named Matlab function, named with a nul terminated byte string.
 */
pub fn call_named_nul(f: &CStr, nargout: usize, rhs: &[&mxArray])
	-> Option<Box<[Option<MxArray>]>>
{
	let lhs = vec![None; nargout].into_boxed_slice();

	let ret = unsafe {
		mexCallMATLAB(
			lhs.len().try_into().expect("Don't expect 4 billion return values"),
			lhs.as_ptr() as *mut *mut mxArray,
			rhs.len().try_into().expect("Don't expect 4 billion arguments"),
			rhs.as_ptr() as *mut *mut mxArray,
			f.as_ptr() as *const i8
		)
	};

	if ret == 0 {
		Some(lhs)
	} else {
		None
	}
}


/**
 * Typed pointer representing a MATLAB `function_handle`
 *
 * Can be created from an mxArray with the correct class, and can then be used to call
 * back into matlab.
 */
#[repr(transparent)]
#[derive(Debug)]
pub struct Function<F: MatlabPtr>(F);

impl<F> MatlabClass for Function<F> where F: MatlabPtr {
	const CLASS_ID: ClassID = ClassID::Function;
	const COMPLEXITY: bool = false;
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum FunctionCallError {
	BadEncoding,
	CallFailed,
}

impl<'a> FromMatlab<'a> for Function<&'a mxArray> {
	fn from_matlab(mx: &'a mxArray) -> Result<Self, FromMatlabError> {
		Self::correct_class(mx)?;

		if mx.numel() != 1 {
			return Err(FromMatlabError::Size);
		}

		Ok(Function(mx))
	}
}

// Bytestring to call feval with without having to construct the bytestring every time.
const FEVAL: &[u8; 6] = b"feval\0";

impl<F> Function<F> where F: MatlabPtr + AsRef<mxArray> + std::fmt::Debug {
	/**
	 * Call the function handle with the specified input arguments and number of
	 * output arguments.
	 */
	// TODO: Add a check for too many arguments (limited by fifty in, fifty out).
	pub fn call(&self, nargout: usize, rhs: &[&mxArray])
		-> Option<Box<[Option<MxArray>]>>
	{
		// Since we're calling the function handle via feval, we need to
		// "push_front" the function_handle onto the left-hand-side array
		let rhs = {
			let mut v = Vec::with_capacity(rhs.len() + 1);
			v.push(self.0.as_ref());
			v.extend(rhs.iter().map(|x| x.as_ref()));
			v
		};

		// SAFETY: We already added the nul to FEVAL ourselves
		call_named_nul(unsafe { CStr::from_bytes_with_nul_unchecked(FEVAL) },
			nargout,
			&rhs)
	}
}
