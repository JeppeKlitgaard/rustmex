/*!
 * This module defines the Mappable family of traits. These provide some mapping
 * function, taking a closure, which is applied to every part of some object. These are
 * mostly used on Complex<T>, since it is used by NDarray as a general container type.
 */
use num_complex::Complex;

/**
 * Convert some object into another object via some shared behaviour for each of its
 * parts.
 */
pub(crate) trait Mappable<A, B> {
	/// The return type. Can be Self, but with a modified inner type, or something
	/// entirely different
	type Target;

	/// Perform the conversion by value
	fn map<F>(self, f: F) -> Self::Target where F: Fn(A) -> B;
}


impl<A, B> Mappable<A, B> for Complex<A> {
	type Target = Complex<B>;
	fn map<F>(self, f: F) -> Self::Target where F: Fn(A) -> B {
		Self::Target {
			re: f(self.re),
			im: f(self.im)
		}
	}
}

/**
 * Mutate some object via some shared behaviour applied to each of its parts.
 */
pub(crate) trait MutMappable<T> {
	/// Perform the mutation
	fn map_mut<F>(&mut self, f: F) where F: Fn(&mut T);
}

impl<T> MutMappable<T> for Complex<T> {
	fn map_mut<F>(&mut self, f: F) where F: Fn(&mut T) {
		f(&mut self.re);
		f(&mut self.im);
	}
}

/**
 * Derive a new type through applying some function to each of its constituent parts by
 * reference.
 */
pub(crate) trait RefMappable<A, B> {
	type Target;
	/// Perform the derivation
	fn map_ref<F>(&self, f: F) -> Self::Target where F: Fn(&A) -> B;
}

impl<A, B> RefMappable<A, B> for Complex<A> {
	type Target = Complex<B>;

	fn map_ref<F>(&self, f: F) -> Self::Target where F: Fn(&A) -> B {
		Self::Target {
			re: f(&self.re),
			im: f(&self.im)
		}
	}
}
